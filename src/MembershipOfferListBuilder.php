<?php

namespace Drupal\membership_offer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Membership Offer entities.
 *
 * @ingroup membership
 */
class MembershipOfferListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Membership Offer ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\membership_offer\Entity\MembershipOffer */
    $row['id'] = $entity->id();
    $row['name'] = new Link(
      $entity->label(),
      new Url(
        'entity.membership_offer.edit_form', array(
          'membership_offer' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
